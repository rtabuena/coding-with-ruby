class Gadget
  def initialize(username, password)
    @username = username
    @password = password
    @production_number = "#{("a".."z").to_a.sample} - #{rand(1..999)}"
  end

  def to_s
    "Gadget #{@production_number} has the username #{@username}. It is made from the #{self.class} class and it has the ID #{self.object_id}"
  end

 # GETTER METHOD
  # def username
  #   @username
  # end

  # def production_number
  #   @production_number
  # end
  # GETTER METHOD

  # SETTER METHOD OR WRITER METHOD
  def password=(password)
    @password = password
  end

  def username=(username)
    @username = username
  end

  # SHORTCUT ACCESSOR METHODS
  # ACCESSOR METHOD - ACCESS THE VALUE
  attr_accessor :username # create getter and setter
  attr_reader   :production_number # create getter only
  attr_writer   :password # crete writer method only



end

phone = Gadget.new
laptop = Gadget.new


puts laptop.to_s

phone.username=("ruby_man") # SHORT HAND
phone.username = "ruby_man"
puts phone.to_s



# instance variable is not accessible
# the only way to communicate with it is through method
#
