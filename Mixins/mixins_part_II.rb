# The best practice is to store each module and
# each class definition within its own seperate file.

# modules naming convention always end in the word able.
# because modules add some kind of functionality
# has a relationship
module Purchaseable

  def purchase(item)   # the self keyword is not going to be prefixed because we want the module to be available on the objects of the class.
    "#{item} has been purchased!"
  end

end

class Bookstore
  include Purchaseable

end

class Supermarket
  include Purchaseable

end

class CornerMart < Supermarket
end

barnes_and_noble = Bookstore.new
p barnes_and_noble.purchase("Atlas Shrugged")

shoprite = Supermarket.new
p shoprite.purchase("Ice cream")

quickstop = CornerMart.new
p quickstop.purchase("Slim Jim")
