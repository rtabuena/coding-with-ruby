module Announcer
  def who_am_i
    "The name of this class is #{self}"
  end
end

class Dog
  extend Announcer #extend keyword takes module methods and it adds them to the class level rather than to the instance level
end

class Cat
  extend Announcer
end

watson = Dog.new
# p watson.who_am_i # this code will trigger an error

p Dog.who_am_i
p Cat.who_am_i



# SUMMARY

# Include takes the modules methods and it mixes them in such that all of the objects created from that class will have access to the module methods
# Prepend does the exact same thing, but it makes the module methods have greater precedence than the methods defined on the class.
# Extend takes those module methods and it adds them to the class level rather than to the instance level.
