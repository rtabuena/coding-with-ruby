module Purchaseable
  def purchase(item)
    "#{item} has been purchased!"
  end

end

class Bookstore
  include Purchaseable

  def purchase(item) # this method will take precendence over the purchage method in Purchaseable
    "You bought a copy of #{item} at the bookstore!"
  end
end

class Supermarket
  include Purchaseable
end

class CornerMart < Supermarket
  def purchase(item)
    "Yay! A great purchase of #{item} from your corner mart!"
  end
end



p Bookstore.ancestors # [Bookstore, Purchaseable, Object, Kernel, BasicObject]

bn = Bookstore.new
bn.purchase("1984")

# Ancestors simply returns an array of the lookup path, the order or the path in which Ruby is going to lookfor a method.
# And that order is always going to proceed up the inheritance hierarchy.
# It's always going to look in the class first, then in the classes modules.
# Then if it's unable to find it, it's going to proceed to the superclass and in the super classes amodules.
# And it's going to continue that process up until it reaches the end of the chain.
# If it reaches the end of the chain, by the way, and it's not able to find whatever method it is that's
# being called on the object, that's the point at which Ruby is going to throw an error and the program
# is going to give you that big, bright read error message. But otherwise, Ruby is going to continue searching until it until it finds something.
# And at that point it's going to stop. And that's the method that's going to take precedence.
# That's the method that's going to override all the other ones and be run.
