module Purchaseable
  def purchase(item)
    "#{item} has been purchased!"
  end

end

class Bookstore
  # include Purchaseable
  prepend Purchaseable # prepend keyword means greater precedence

  def purchase(item)
    "You bought a copy of #{item} at the bookstore!"
  end
end


p Bookstore.ancestors
bn = Bookstore.new
p bn.purchase("1984") # OUTPUT "1984 has been purchased!"
