class Candidate
  attr_accessor :name, :age, :occupation, :hobby, :birthplace
  def initialize(name, details = {})

    defaults = {age: 35, occupation: "Candidate", hobby: "Running"}
    defaults.merge!(details)

    @name = name
    @age = defaults[:age]
    @occupation = defaults[:occupation]
    @hobby = defaults[:hobby]
    @birthplace = defaults[:birthplace]
  end


end

info = {age: 53, occupation: "Banker", hobby: "Fishing"}
senator = Candidate.new("Mr. Smith", age: 53, occupation: "Banker", hobby: "Fishing")
p senator.age
puts senator.occupation
puts senator.hobby
puts senator.name
