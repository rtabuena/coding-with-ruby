class Employee

  attr_accessor :age
  attr_reader :name

  def initialize(name, age)
    @name = name
    @age = age
  end

  def introduce
    "Hi, my name is #{name} and I am #{age} years old"
  end
end


class Manager < Employee

  attr_reader :rank

  def initialize(name, age, rank)
    super(name, age)
    @rank = rank
  end

  def yell
    "Who's the boss? I'm the boss!"
  end

  def introduce
    result = super
    result += " I'm also a manager!"
  end
end

class Worker < Employee
  def clock_in(time)
    "Starting my shift at #{time}"
  end

  def yell
    "I'm working! I'm working!"
  end
end


bob = Manager.new("Bob", 48, "Senior Vice President")
dan = Worker.new("Daniel", 36)


p bob.introduce
p dan.introduce

# OUTPUT
# "Hi, my name is Bob and I am 48 years old"
# "Hi, my name is Daniel and I am 36 years old"

puts Manager < Employee # if manage inherits employee class
puts Worker < Employee
p Manager.ancestors

puts bob.is_a?(Manager)
puts dan.is_a?(Worker)
puts bob.instance_of?(Employee) # check only for actual class
